console.log("Hello, world!");

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	
	let firstname = "John"
	let lastname = "Smith"
	
	let johnName = "First Name: " + firstname;
	console.log(johnName);

	let familyName = "Last Name: " + lastname;
	console.log(familyName);

	let age1 = 30

	let johnAge = "Age: " + age1;
	console.log(johnAge);

	let johnHobbies = "Hobbies :";
	console.log(johnHobbies);

	let hobbies =['Biking', 'Mountain Climbing', 'Swimming'];
	console.log(hobbies);

	let address = "Work Address: ";
	console.log(address);

	let workAddress ={
		houseNumber: '32',
		street: 'Washington',
		city: 'Lincoln',
		state: 'Nebraska',
	}

	console.log(workAddress);

	let fullName = "Steve Rogers";
	let name = ("My full name is " + fullName);

	console.log(name);

	let age = 40;
	let steveAge = ("My current age is: " + age);

	console.log(steveAge);

	let steveFriends = ("My Friends are: ");
	console.log(steveFriends);

	let friends = ["Tony","Bruce","Thor", "Natasha", "Clint", "Nick"];
	console.log(friends);

	let myProfile = ("My Full Profile: ")
	console.log(myProfile);

	let profile = {
		username:'captain_america',
		fullName: 'Steve Rogers',
		age: 40,
		isActive: false,
	}
	console.log(profile);

	let bestFriend = "Bucky Barnes";
	let myBest = "My bestfriend is: " + bestFriend;

	console.log(myBest);

	const found = "Arctic Ocean";
	lastLocation = "I was found in: ";
	console.log(lastLocation + found);